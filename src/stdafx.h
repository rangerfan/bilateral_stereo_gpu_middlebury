#include "stdio.h"
#include "stdlib.h"
#include <iostream>

#include <omp.h>

#include <cuda.h>
#include <cuda.h>
#include <cuda_runtime.h>

#include "opencv2/core.hpp"
#include "opencv2/features2d.hpp"
#include "opencv2/xfeatures2d.hpp"
#include "opencv2/opencv.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/cudev/ptr2d/gpumat.hpp"
#include "opencv2/core/cuda.hpp"
#include "opencv/highgui.h"

inline void CheckComputeCapability(void)
{
	/// <<<Obtain the information of the devices>>>
	std::cout << ">>> GPU Properties" << std::endl << std::endl;
	int deviceCount;
	// Declare a variable to enumerate the devices
	cudaGetDeviceCount(&deviceCount);
	// Get the number of the device(s)
	std::cout << "The host system has " << deviceCount << " device(s)" << std::endl;
	std::cout << std::endl;
	cudaDeviceProp deviceProp;
	// Declare a variable to get the properties of the cuda device
	for (int device = 0; device < deviceCount; device++)
	{
		cudaGetDeviceProperties(&deviceProp, device);
		// The first variable is the address of the deviceProp
		// The second variable is the device we want to enumerate
		std::cout << deviceProp.name << ": " << std::endl;
		std::cout << "The compute capability " << deviceProp.major << "." << deviceProp.minor << ", " << deviceProp.multiProcessorCount << " Multi-Processors." << std::endl;
		// deviceProp.multiProcessorCount is used to get the number of multiprocessors 
	}
	std::cout << std::endl << "GPU Properties <<<" << std::endl << std::endl << std::endl;
	/// <<<Obtain the information of the devices>>>
}




inline int iDivUp(int a, int b)
{
	return ((a % b) != 0) ? (a / b + 1) : (a / b);
}

//inline void RoadParameterVectorSetting(const int dataset_num){
//	if (dataset_num == 1){
//		coefficient3[0] = 57.1402; coefficient3[1] = 0.2149; coefficient3[2] = 20;
//	}
//	else if (dataset_num == 2){
//		coefficient3[0] = 96.9328; coefficient3[1] = 0.1619; coefficient3[2] = 20;
//	}
//	else{
//		coefficient3[0] = 229.9465; coefficient3[1] = 0.1611; coefficient3[2] = 30;
//	}
//}
//
//inline void IntegralImageInitialisation(
//	unsigned char* l, unsigned char* r,
//	unsigned long* l_integral_image, unsigned long* r_integral_image,
//	unsigned long* l_sqr_integral_image, unsigned long* r_sqr_integral_image,
//	const int umax, const int vmax)
//{
//
//	l_integral_image[0] = (unsigned long)l[0];
//	r_integral_image[0] = (unsigned long)r[0];
//	l_sqr_integral_image[0] = (unsigned long)l[0] * (unsigned long)l[0];
//	r_sqr_integral_image[0] = (unsigned long)r[0] * (unsigned long)r[0];
//
//	for (int j = 1; j < umax; j++) {
//
//		l_integral_image[j] = l_integral_image[j - 1] + (unsigned long)l[j];
//		r_integral_image[j] = r_integral_image[j - 1] + (unsigned long)r[j];
//
//		l_sqr_integral_image[j] = l_sqr_integral_image[j - 1] + (unsigned long)l[j] * (unsigned long)l[j];
//		r_sqr_integral_image[j] = r_sqr_integral_image[j - 1] + (unsigned long)r[j] * (unsigned long)r[j];
//	}
//
//	for (int i = 1; i < vmax; i++) {
//
//		l_integral_image[i*umax] = l_integral_image[i*umax - umax] + (unsigned long)l[i*umax];
//		r_integral_image[i*umax] = r_integral_image[i*umax - umax] + (unsigned long)r[i*umax];
//
//		l_sqr_integral_image[i*umax] = l_sqr_integral_image[i*umax - umax] + (unsigned long)l[i*umax] * (unsigned long)l[i*umax];
//		r_sqr_integral_image[i*umax] = r_sqr_integral_image[i*umax - umax] + (unsigned long)r[i*umax] * (unsigned long)r[i*umax];
//	}
//
//	for (int i = 1; i < vmax; i++) {
//		for (int j = 1; j < umax; j++) {
//			l_integral_image[i*umax + j] = l_integral_image[(i - 1)*umax + j] + l_integral_image[i*umax + j - 1]
//				- l_integral_image[(i - 1)*umax + j - 1] + (unsigned long)l[i*umax + j];
//
//			r_integral_image[i*umax + j] = r_integral_image[(i - 1)*umax + j] + r_integral_image[i*umax + j - 1]
//				- r_integral_image[(i - 1)*umax + j - 1] + (unsigned long)r[i*umax + j];
//
//			l_sqr_integral_image[i*umax + j] = l_sqr_integral_image[(i - 1)*umax + j] + l_sqr_integral_image[i*umax + j - 1]
//				- l_sqr_integral_image[(i - 1)*umax + j - 1] + (unsigned long)l[i*umax + j] * (unsigned long)l[i*umax + j];
//
//			r_sqr_integral_image[i*umax + j] = r_sqr_integral_image[(i - 1)*umax + j] + r_sqr_integral_image[i*umax + j - 1]
//				- r_sqr_integral_image[(i - 1)*umax + j - 1] + (unsigned long)r[i*umax + j] * (unsigned long)r[i*umax + j];
//		}
//	}
//}
//
