#include "stdafx.h"
#include "stdio.h"
#include "stdlib.h"
#include <iostream>
#include <omp.h>
#include <cuda.h>
#include <cuda_runtime.h>
#include "opencv2/core.hpp"
#include "opencv2/features2d.hpp"
#include "opencv2/xfeatures2d.hpp"
#include "opencv2/opencv_modules.hpp"
#include "opencv2/opencv.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/cudev/ptr2d/gpumat.hpp"
#include "opencv2/core/cuda.hpp"

#define aggsize 7
#define sigma1 1.75
#define sigma2 3.5
#define tau 2
#define Block_x 8
#define Block_y 16

using namespace std;
using namespace cv;

struct information
{
	char left_image_path[256]; 
	char right_image_path[256];
	char output_path[256];
	char left_output_path[256];
	char right_output_path[256];
	int dmax;
}parameter_information;






texture<unsigned char, 2, cudaReadModeElementType> l_tex;
texture<unsigned char, 2, cudaReadModeElementType> r_tex;


float coefficient1[2 * aggsize + 1][2 * aggsize + 1];
float coefficient2[256];
//float coefficient3[3];

__device__ __constant__ float d_coefficient1[2 * aggsize + 1][2 * aggsize + 1];
__device__ __constant__ float d_coefficient2[256];



inline void information_setup(const int frame_num)
{
	if (frame_num == 1){
		strcpy_s(parameter_information.left_image_path, "..//..//middlebury//middlebury2001//tsukuba//scene1.row3.col3.ppm");
		strcpy_s(parameter_information.right_image_path, "..//..//middlebury//middlebury2001//tsukuba//scene1.row3.col5.ppm");
		strcpy_s(parameter_information.output_path, "..//..//output//tsukuba_final.tiff");
		strcpy_s(parameter_information.left_output_path, "..//..//output//tsukuba_l.tiff");
		strcpy_s(parameter_information.right_output_path, "..//..//output//tsukuba_r.tiff");
		parameter_information.dmax = 30;
	}
	else if (frame_num == 2){
		strcpy_s(parameter_information.left_image_path, "..//..//middlebury//middlebury2001//venus//im2.ppm");
		strcpy_s(parameter_information.right_image_path, "..//..//middlebury//middlebury2001//venus//im6.ppm");
		strcpy_s(parameter_information.output_path, "..//..//output//venus_final.tiff");
		strcpy_s(parameter_information.left_output_path, "..//..//output//venus_l.tiff");
		strcpy_s(parameter_information.right_output_path, "..//..//output//venus_r.tiff");
		parameter_information.dmax = 20;
	}
	else if (frame_num == 3){
		strcpy_s(parameter_information.left_image_path, "..//..//middlebury//middlebury2003//conesQ//im2.ppm");
		strcpy_s(parameter_information.right_image_path, "..//..//middlebury//middlebury2003//conesQ//im6.ppm");
		strcpy_s(parameter_information.output_path, "..//..//output//conesQ_final.tiff");
		strcpy_s(parameter_information.left_output_path, "..//..//output//conesQ_l.tiff");
		strcpy_s(parameter_information.right_output_path, "..//..//output//conesQ_r.tiff");
		parameter_information.dmax = 60;
	}
	else if (frame_num == 4){
		strcpy_s(parameter_information.left_image_path, "..//..//middlebury//middlebury2003//teddyQ//im2.ppm");
		strcpy_s(parameter_information.right_image_path, "..//..//middlebury//middlebury2003//teddyQ//im6.ppm");
		strcpy_s(parameter_information.output_path, "..//..//output//teddyQ_final.tiff");
		strcpy_s(parameter_information.left_output_path, "..//..//output//teddyQ_l.tiff");
		strcpy_s(parameter_information.right_output_path, "..//..//output//teddyQ_r.tiff");
		parameter_information.dmax = 50;
	}
	else if (frame_num == 5) {
		strcpy_s(parameter_information.left_image_path, "..//..//middlebury//sawtooth////im2.ppm");
		strcpy_s(parameter_information.right_image_path, "..//..//middlebury//sawtooth////im6.ppm");
		strcpy_s(parameter_information.output_path, "..//..//output//sawtooth.tiff");
		strcpy_s(parameter_information.left_output_path, "..//..//output//sawtooth.tiff");
		strcpy_s(parameter_information.right_output_path, "..//..//output//sawtooth.tiff");
		parameter_information.dmax = 30;
	}
	else{
		std::cout << "Error occurs in information setup process!" << std::endl;
	}



}




__global__ void memorisation(
	float* d_mu_l1, float* d_mu_l2,
	float* d_mu_r1, float* d_mu_r2,
	float* d_sigma_l1, float* d_sigma_l2,
	float* d_sigma_r1, float* d_sigma_r2,
	const int rad, const int block_pixels, 
	const int vmax, const int umax)
{
	int x = blockDim.x*blockIdx.x + threadIdx.x;
	int y = blockDim.y*blockIdx.y + threadIdx.y;
	if ((x >= 1 + rad) && (x <= umax - 2 - rad) && (y >= 1 + rad) && (y <= vmax - 2 - rad))
	{
		float l_sum = 0; float l_sqr_sum = 0;
		float r_sum = 0; float r_sqr_sum = 0;
#pragma unroll
		for (int p = -rad; p <= rad; p++){
			for (int q = -rad; q <= rad; q++){
				float l_intensity = (float)tex2D(l_tex,
					(x + q), (y + p));
				float r_intensity = (float)tex2D(r_tex,
					(x + q), (y + p));
				l_sum += l_intensity; r_sum += r_intensity;
				l_sqr_sum += l_intensity*l_intensity;
				r_sqr_sum += r_intensity*r_intensity;
			}
		}
		float dl_mu = l_sum / (float)block_pixels;
		float dr_mu = r_sum / (float)block_pixels;
		d_mu_l1[y*umax + x] = dl_mu;
		d_mu_r2[y*umax + umax - 1 - x] = dl_mu;
		d_mu_r1[y*umax + x] = dr_mu;
		d_mu_l2[y*umax + umax - 1 - x] = dr_mu;
		d_sigma_l1[y*umax + x] = sqrt(l_sqr_sum - l_sum*dl_mu);
		d_sigma_r2[y*umax + umax - 1 - x] = d_sigma_l1[y*umax + x];
		d_sigma_r1[y*umax + x] = sqrt(r_sqr_sum - r_sum*dr_mu);
		d_sigma_l2[y*umax + umax - 1 - x] = d_sigma_r1[y*umax + x];
	}
}

__global__ void cost_computation(
	float* d_mu_l, float* d_mu_r,
	float* d_sigma_l, float* d_sigma_r, 
	float* d_l_volume, float* d_r_volume,
	const int dmax, const int rad, const int block_pixels,
	const int vmax, const int umax, const float t){
	int x = blockDim.x*blockIdx.x + threadIdx.x;
	int y = blockDim.y*blockIdx.y + threadIdx.y;
	if ((x >= 1 + rad) && (x <= umax - 2 - rad) && (y >= 1 + rad) && (y <= vmax - 2 - rad)){
		int kmax = ((x - (1 + rad)) < dmax) ? (x - (1 + rad)) : dmax;
		float max = t;
		float cost; float disp_val = 0;;
		float cost_sum = 0;
		int ul = x; int v = y;
		float mu_l = d_mu_l[v*umax + ul];
		float sigma_l = d_sigma_l[v*umax + ul];
		if (sigma_l != 0){
#pragma unroll
			for (int k = 0; k <= kmax; k++){
				cost = 0;
				int ur = ul - k;
				float mu_r = d_mu_r[v*umax + ur];
				float sigma_r = d_sigma_r[v*umax + ur];
				if (sigma_r != 0){
					float sum = 0;
					for (int p = -rad; p <= rad; p++){
						for (int q = -rad; q <= rad; q++){
							float l_intensity = (float)tex2D(l_tex,
								(ul + q), (v + p));
							float r_intensity = (float)tex2D(r_tex,
								(ur + q), (v + p));
							sum += (l_intensity*r_intensity);
						}
					}
					cost = (sum - (float)block_pixels*mu_l*mu_r) / (sigma_l*sigma_r);
				}
				cost_sum += cost;
				d_l_volume[(umax*vmax)*(k + tau) + (v*umax + ul)] = cost;
				d_r_volume[(umax*vmax)*(k + tau) + (v*umax + ur)] = cost;
			}
		}
	}
}


__global__ void cost_aggregation(
	float* d_l_volume, float* d_r_volume,
	float* d_l_volume_filtered, float* d_r_volume_filtered,

	const int dmax, const int rad, const int block_pixels,
	const int vmax, const int umax, const float t)
{
	int x = blockDim.x*blockIdx.x + threadIdx.x;
	int y = blockDim.y*blockIdx.y + threadIdx.y;
	if ((x >= 1 + rad + 1) && (x <= umax - 2 - rad - 1) && (y >= 1 + rad + 1) && (y <= vmax - 2 - rad - 1)){
		int kmax = ((x - (1 + rad + 1)) < dmax) ? (x - (1 + rad + 1)) : dmax;
		for (int k = 0; k <= kmax; k++){
			float sum1 = 0;
			float sum2 = 0;
			float cost_center = d_l_volume[(umax*vmax)*(k + tau) + (y*umax + x)];
			int intensity_center = (int)tex2D(l_tex, x, y);
			for (int p = -aggsize; p <= aggsize; p++) {
				for (int q = -aggsize; q <= aggsize; q++) {
					float cost_neighbour = d_l_volume[(umax*vmax)*(k + tau) + ((y + p)*umax + (x + q))];
					float cost_difference = (cost_neighbour - cost_center);
					float weighting2 = d_coefficient2[(int)abs(intensity_center - (int)tex2D(l_tex, x + q, y + p))];
					float weighting1 = d_coefficient1[p + aggsize][q + aggsize];;
					float weighting = weighting1*weighting2;
					sum1 += weighting;
					sum2 += weighting*cost_neighbour;
				}
			}
			float new_cost = sum2 / sum1;
			d_l_volume_filtered[(umax*vmax)*(k + tau) + (y*umax + x)] = new_cost;
			d_r_volume_filtered[(umax*vmax)*(k + tau) + (y*umax + x - k)] = new_cost;
		}
	}
}


__global__ void disparity_optimisation(float* d_disp_left, float* d_disp_right,
	float* d_l_volume, float* d_r_volume, float* d_l_volume_filtered, float* d_r_volume_filtered,
	const int dmax, const int dmin, const int rad, const int vmax, const int umax, const float t)
{
	int x = blockDim.x*blockIdx.x + threadIdx.x;
	int y = blockDim.y*blockIdx.y + threadIdx.y;
	if ((x >= 1 + rad) && (x <= umax - 2 - rad) && (y >= 1 + rad) && (y <= vmax - 2 - rad)){
		float cost_l = 0; float cost_r = 0;
		float cost_l_filtered = 0; float cost_r_filtered = 0;
		float costl1 = 0; float costl2 = 0;
		float costr1 = 0; float costr2 = 0;
		float max_l = t; float max_r = t;

		float disp_val_l = 0; float disp_val_r = 0;
		for (int k = dmin; k < dmax; k++)
		{
			cost_l = d_l_volume[(umax*vmax)*(k + tau) + (y*umax + x)];
			cost_r = d_r_volume[(umax*vmax)*(k + tau) + (y*umax + x)];
			cost_l_filtered = d_l_volume_filtered[(umax*vmax)*(k + tau) + (y*umax + x)];
			cost_r_filtered = d_r_volume_filtered[(umax*vmax)*(k + tau) + (y*umax + x)];
			if ((cost_l_filtered > max_l)){
				max_l = cost_l_filtered;
				disp_val_l = k;
			}
			if ((cost_r_filtered > max_r)){
				max_r = cost_r_filtered;
				disp_val_r = k;
			}
		}
		d_disp_left[y*umax + x] = disp_val_l;
		d_disp_right[y*umax + x] = disp_val_r;
	}
}

__global__ void disparity_refinement(float* d_disp_left, float* d_disp_right,
	float* d_l_volume, float* d_r_volume, float* d_l_volume_filtered, float* d_r_volume_filtered,
	const int threshold, const int dmax, const int rad, const int vmax, const int umax)
{
	int x = blockDim.x*blockIdx.x + threadIdx.x;
	int y = blockDim.y*blockIdx.y + threadIdx.y;
	if ((x >= 1 + rad) && (x <= umax - 2 - rad) && (y >= 1 + rad) && (y <= vmax - 2 - rad)){
		int dl = d_disp_left[y*umax + x];
		int dr = d_disp_right[y*umax + x - dl];
		if (abs(dl - dr) > threshold){
			d_disp_left[y*umax + x] = 0;
		}
		else{

			float costl = d_l_volume_filtered[(umax*vmax)*(dl - 1+tau) + (y*umax + x)];
			float costr = d_l_volume_filtered[(umax*vmax)*(dl + 1+tau) + (y*umax + x)];
			float costm = d_l_volume_filtered[(umax*vmax)*(dl+tau) + (y*umax + x)];
			if ((costm > costl) && (costm > costr)){
				float d = (costl - costr) / (2 * costl + 2 * costr - 4 * costm) + dl;
				d_disp_left[y*umax + x] = d;
			}
		}
	}
}



int main(int, char)
{

	const int frame_num = 3;
	CheckComputeCapability();
	information_setup(frame_num);
	



	for (int i = -aggsize; i <= aggsize; i++) {
		for (int j = -aggsize; j <= aggsize; j++) {
			coefficient1[i + aggsize][j + aggsize] = exp(-sqrt((float)i*(float)i + (float)j*(float)j) / (2 * sigma1 * sigma1));
		}
	}
	for (int i = 0; i < 256; i++){
		coefficient2[i]= exp(-abs((float)i) / (2 * sigma2 * sigma2));
	}

	cudaMemcpyToSymbol(d_coefficient1, coefficient1, (2 * aggsize + 1)*(2 * aggsize + 1) * sizeof(float));
	cudaMemcpyToSymbol(d_coefficient2, coefficient2, 256 * sizeof(float));

	

	typedef double clock_t;
	clock_t t1, t2, t3;
	double d1, d2, d3;


	cv::Mat left_img_mat = imread(parameter_information.left_image_path, CV_LOAD_IMAGE_GRAYSCALE);
	cv::Mat right_img_mat = imread(parameter_information.right_image_path, CV_LOAD_IMAGE_GRAYSCALE);


	const int vmax = left_img_mat.rows;
	const int umax = left_img_mat.cols;
	unsigned char* l = left_img_mat.data;
	unsigned char* r = right_img_mat.data;


	const int pixel_number = vmax*umax;
	const int float_memsize = sizeof(float)*pixel_number;
	const int ulong_memsize = sizeof(unsigned long)*pixel_number;
	const int uchar_memsize = sizeof(unsigned char)*pixel_number;
	const float min_cost = 0.0f; const int rad = 1;
	const int dmax = parameter_information.dmax; const int dmin = 0;
	const float scale = 1000;
	const int block_pixels = (rad * 2 + 1)*(rad * 2 + 1);


	float* disp_left = (float*)calloc(pixel_number, sizeof(float*)); cv::Mat left_disp_mat(vmax, umax, CV_32F, disp_left);
	float* disp_right = (float*)calloc(pixel_number, sizeof(float*)); cv::Mat right_disp_mat(vmax, umax, CV_32F, disp_right);


	float* d_disp_left; cudaMalloc((void **)&d_disp_left, float_memsize);
	float* d_disp_right; cudaMalloc((void **)&d_disp_right, float_memsize);


	float* d_l_volume; cudaMalloc((void **)&d_l_volume, (dmax + 2 * tau)*float_memsize);
	float* d_r_volume; cudaMalloc((void **)&d_r_volume, (dmax + 2 * tau)*float_memsize);

	float* d_l_volume_filtered; cudaMalloc((void **)&d_l_volume_filtered, (dmax + 2 * tau)*float_memsize);
	float* d_r_volume_filtered; cudaMalloc((void **)&d_r_volume_filtered, (dmax + 2 * tau)*float_memsize);



	float* d_mu_l1, *d_mu_r1, *d_sigma_l1, *d_sigma_r1;
	float* d_mu_l2, *d_mu_r2, *d_sigma_l2, *d_sigma_r2;
	cudaMalloc((void **)&d_mu_l1, float_memsize);
	cudaMalloc((void **)&d_mu_r1, float_memsize);
	cudaMalloc((void **)&d_sigma_l1, float_memsize);
	cudaMalloc((void **)&d_sigma_r1, float_memsize);
	cudaMalloc((void **)&d_mu_l2, float_memsize);
	cudaMalloc((void **)&d_mu_r2, float_memsize);
	cudaMalloc((void **)&d_sigma_l2, float_memsize);
	cudaMalloc((void **)&d_sigma_r2, float_memsize);

	cudaEvent_t start, stop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);
	cudaEventRecord(start, NULL);

	cudaChannelFormatDesc desc_l = cudaCreateChannelDesc<unsigned char>();
	cudaChannelFormatDesc desc_r = cudaCreateChannelDesc<unsigned char>();
	cudaArray* d_l_texture, *d_r_texture;
	cudaMallocArray(&d_l_texture, &desc_l, umax, vmax);
	cudaMallocArray(&d_r_texture, &desc_r, umax, vmax);
	cudaMemcpyToArray(d_l_texture, 0, 0, l, uchar_memsize, cudaMemcpyHostToDevice);
	cudaMemcpyToArray(d_r_texture, 0, 0, r, uchar_memsize, cudaMemcpyHostToDevice);
	cudaBindTextureToArray(l_tex, d_l_texture, desc_l);
	cudaBindTextureToArray(r_tex, d_r_texture, desc_r);

	dim3 threads = dim3(Block_x, Block_y);
	dim3 blocks = dim3(iDivUp(umax, threads.x), iDivUp(vmax, threads.y));
	memorisation <<< blocks, threads >>> (d_mu_l1, d_mu_l2, d_mu_r1, d_mu_r2, d_sigma_l1, d_sigma_l2, d_sigma_r1, d_sigma_r2, rad, block_pixels, vmax, umax);
	blocks = dim3(iDivUp(umax, threads.x), iDivUp(vmax, threads.y));
	cost_computation <<< blocks, threads >>> (d_mu_l1, d_mu_r1, d_sigma_l1, d_sigma_r1, d_l_volume, d_r_volume, dmax, rad, block_pixels, vmax, umax, min_cost);
	cost_aggregation <<< blocks, threads >>> (d_l_volume, d_r_volume, d_l_volume_filtered, d_r_volume_filtered, dmax, rad, block_pixels,	vmax, umax, min_cost);
	disparity_optimisation << < blocks, threads >> > (d_disp_left, d_disp_right, d_l_volume, d_r_volume, d_l_volume_filtered, d_r_volume_filtered, dmax, dmin, rad, vmax, umax, min_cost);
	disparity_refinement << < blocks, threads >> >(d_disp_left, d_disp_right, d_l_volume, d_r_volume, d_l_volume_filtered, d_r_volume_filtered, 1, dmax, rad, vmax, umax);

	cudaEventRecord(stop, NULL);
	cudaEventSynchronize(stop);
	float msecTotal = 1.0f;
	cudaEventElapsedTime(&msecTotal, start, stop);
	std::cout << msecTotal << std::endl;

	cudaMemcpy(disp_left, d_disp_left, float_memsize, cudaMemcpyDeviceToHost);
	//cudaMemcpy(disp_right, d_disp_right, float_memsize, cudaMemcpyDeviceToHost);
	

	cv::Mat left_output; left_disp_mat.convertTo(left_output, CV_16UC4, scale);
	cv::imwrite(parameter_information.left_output_path, left_output);
	//cv::Mat right_output; right_disp_mat.convertTo(right_output, CV_16UC4, scale);
	//cv::imwrite(parameter_information.right_output_path, right_output);



	return 0;
}


